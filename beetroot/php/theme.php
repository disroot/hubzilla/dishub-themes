<?php

/**
 *   * Name: Beetroot
 *   * Description: Redbasic Derived theme
 *   * Version: 1.0
 *   * MinVersion: 2.3.1
 *   * MaxVersion: 6.0
 *   * Author: Antilopa
 *   * Compat: Red [*]
 *
 */

function beetroot_init(&$a) {

    App::$theme_info['extends'] = 'redbasic';


}
